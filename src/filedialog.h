#pragma once
#include "imgui.h"

#include <algorithm>
#include <filesystem>
#include <optional>
#include <string>
#include <vector>

class FileDialog
{
public:
    auto updatePathAndExtension( std::filesystem::path const& path,
                                 std::vector<std::string> const& extensionFilters ) -> void;


    auto draw() -> void;

    auto requestOpen() -> void;

    auto isActive() -> bool { return openFile_; }

    auto popPath() -> std::filesystem::path;

    auto pathEmpty() -> bool { return pathToReturn_.empty(); }


private:
    auto updatePath_( std::filesystem::path const& path ) -> void;


    auto selectedDirectory_() -> std::optional<std::filesystem::directory_entry>;

    bool openFile_ = false;

    std::filesystem::path pathToReturn_;
    std::filesystem::path currentDirectoryPath_;

    std::vector<bool> selected_;
    std::vector<std::filesystem::directory_entry> listPath_;
    std::vector<std::string> filenames_;

    std::vector<std::string> fileExtensions_;
};
