#pragma once
// imgui stuff

#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_sdl.h"

#include "implot.h"

#include <string>

template <typename Window>
struct GuardImgui
{
    explicit GuardImgui( Window const &window );
    ~GuardImgui();

    std::string glslVersion = "#version 330";
};

template <typename Window>
GuardImgui<Window>::GuardImgui( Window const &window )
{
    ImGui::CreateContext();
    // TODO this should check (at compile time) what kind of window it is (SDL2)
    ImGui_ImplSDL2_InitForOpenGL( window.getWindow(), window.getContext() );
    ImGui_ImplOpenGL3_Init( glslVersion.c_str() );

    ImPlot::CreateContext();
}

template <typename Window>
GuardImgui<Window>::~GuardImgui()
{
    ImPlot::DestroyContext();

    // cleanup imgui
    ImGui_ImplOpenGL3_Shutdown();
    // TODO this should check (at compile time) what kind of window it is (SDL2)
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();
}
