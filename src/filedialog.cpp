#include "filedialog.h"

#include <algorithm>
#include <sstream>

#include "range/v3/view.hpp"

auto FileDialog::updatePathAndExtension( const std::filesystem::path& path,
                                         const std::vector<std::string>& extensionFilter ) -> void
{
    fileExtensions_ = extensionFilter;

    updatePath_( path );
}


auto FileDialog::updatePath_( std::filesystem::path const& path ) -> void
{
    listPath_.clear();

    filenames_.clear();
    selected_.clear();

    currentDirectoryPath_ = path;

    auto directoryRange = std::filesystem::directory_iterator( path );

    auto parent = path.parent_path();


    listPath_.push_back( std::filesystem::directory_entry( parent ) );


    auto goodExtensionOrDirectory = [ this ]( auto const& entry ) {
        if ( entry.is_directory() )
        {
            return true;
        }
        else
        {
            if ( fileExtensions_.empty() )
            {
                return true;
            }
            auto extension = entry.path().extension();
            if ( !extension.empty() )
            {
                return std::any_of( begin( fileExtensions_ ), end( fileExtensions_ ),
                                    [ &extension ]( auto const& filter ) //
                                    { return extension.string() == filter; } );
            }
            return false;
        }
    };



    std::copy_if( begin( directoryRange ), end( directoryRange ), back_inserter( listPath_ ),
                  goodExtensionOrDirectory );

    std::sort( next( begin( listPath_ ) ), end( listPath_ ) );


    filenames_.push_back( "../" );

    std::transform( next( begin( listPath_ ) ), end( listPath_ ), back_inserter( filenames_ ),
                    []( auto const& directoryEntry ) //
                    {
                        if ( directoryEntry.is_directory() )
                        {
                            return directoryEntry.path().filename().string() + "/";
                        }
                        else
                        {
                            return directoryEntry.path().filename().string();
                        }
                    } );
    selected_.resize( filenames_.size() );
}


auto FileDialog::selectedDirectory_() -> std::optional<std::filesystem::directory_entry>
{
    if ( !listPath_.empty() )
    {
        auto fileIndexIt = std::find( std::begin( selected_ ), std::end( selected_ ), true );

        auto firstPath = begin( listPath_ );

        if ( fileIndexIt != std::end( selected_ ) )
        {
            auto step = std::distance( std::begin( selected_ ), fileIndexIt );

            return *std::next( firstPath, step );
        }
        else
        {
            return {};
        }
    }
    else
    {
        return {};
    }
}
auto FileDialog::draw() -> void
{
    if ( openFile_ )
    {
        std::vector<const char*> fileView;

        std::transform( begin( filenames_ ), end( filenames_ ), back_inserter( fileView ),
                        []( auto const& str ) //
                        { return str.c_str(); } );

        auto directoryName = currentDirectoryPath_.filename().string();

        ImGui::OpenPopup( "Files" );

        if ( ImGui::BeginPopupModal( "Files", nullptr,
                                     ImGuiWindowFlags_AlwaysAutoResize
                                         | ImGuiWindowFlags_NoSavedSettings ) )
        {
            ImGui::Text( "%s", directoryName.c_str() );

            std::stringstream ss;

            for ( auto const& extension : fileExtensions_ )
            {
                ss << "*" << extension << ";";
            }

            ss << " files";

            auto filterName = ss.str();

            if ( ImGui::BeginListBox( filterName.c_str() ) )
            {
                for ( auto const& [ index, filenameSelectedAndPath ] : ranges::views::enumerate(
                          ranges::views::zip( filenames_, selected_, listPath_ ) ) )
                {
                    auto const& filename = std::get<0>( filenameSelectedAndPath );

                    auto const& selected = std::get<1>( filenameSelectedAndPath );

                    auto const& filepath = std::get<2>( filenameSelectedAndPath );


                    if ( ImGui::Selectable( filename.c_str(), selected ) )
                    {
                        // NOTE: This does not work if a file is selected after a directory
                        // Works good enought for now
                        if ( filepath.is_directory() || !ImGui::GetIO().KeyCtrl )
                        {
                            std::transform( std::begin( selected_ ), std::end( selected_ ),
                                            std::begin( selected_ ),
                                            []( auto const& v ) //
                                            { return false; } );
                        }

                        selected_[ index ] = selected ? false : true;
                    }
                }
                ImGui::EndListBox();
            }

            if ( ImGui::Button( "open" ) )
            {
                //
                auto directoryEntry = selectedDirectory_();
                if ( directoryEntry )
                {
                    if ( directoryEntry->is_directory() )
                    {
                        updatePath_( directoryEntry->path() );
                    }
                    else
                    {
                        pathToReturn_ = directoryEntry->path();
                        openFile_     = false;
                        ImGui::CloseCurrentPopup();
                    }
                }
            }

            if ( ImGui::Button( "close" ) )
            {
                openFile_ = false;
                ImGui::CloseCurrentPopup();
            }

            ImGui::EndPopup();
        }
    }
}

auto FileDialog::requestOpen() -> void
{
    openFile_ = true;
}

auto FileDialog::popPath() -> std::filesystem::path
{
    auto path        = pathToReturn_;
    auto fileIndexIt = std::find( std::begin( selected_ ), std::end( selected_ ), true );

    if ( fileIndexIt != std::end( selected_ ) )
    {
        *fileIndexIt = false;
    }

    auto optFile = selectedDirectory_();
    if ( optFile )
    {
        pathToReturn_ = *optFile;
    }
    else
    {
        pathToReturn_.clear();
    }


    return path;
}
