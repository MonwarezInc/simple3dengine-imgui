#!/bin/sh

export CC=clang
export CXX=clang++

meson build && cd build && ninja
